package com.tsystems.smarttracking.vwskgeo.ste.model;

import org.springframework.data.annotation.Id;

public class Asset
{
    @Id
    public String id;
    public String name;
    public String tagId;
    
    public Asset() {
    }

    public Asset(String name) {
        this.name = name;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return "id=" + id + ", name=" + name;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
}
