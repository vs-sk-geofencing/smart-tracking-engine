package com.tsystems.smarttracking.vwskgeo.ste.model;

public class Event
{
    public static double TEMPERATURE_NON_VALUE = -10000.0;
    
    // mandatory 
    private String source; // event source UUID
    private String tagId; // system wide unique tag ID
    private String assetId; // system wide unique asset ID
    private long timestamp; // original recording stamp, UTC w/o time saving, milliseconds since 01.01.1970
    
    // optional 
    private String tagName; // human readable tag name
    private String assetName; // human readable asset name
    private Position position;
    private String zone;
    private boolean alarm;
    private double temperature = TEMPERATURE_NON_VALUE;
    private long temperatureTS;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(long ts)
    {
        this.timestamp = ts;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String toString()
    {
//        return "Timestamp=" + timestamp + ", latitude=" + latitude + ", longitude=" + longitude + ", accuracy=" + accuracy + ", zone=" + zone;
        return "Timestamp=" + timestamp + ", position=" + position + ", zone=" + zone;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public long getTemperatureTS() {
        return temperatureTS;
    }

    public void setTemperatureTS(long temperatureTS) {
        this.temperatureTS = temperatureTS;
    }

}
