package com.tsystems.smarttracking.vwskgeo.ste.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.smarttracking.vwskgeo.ste.model.Area;
import com.tsystems.smarttracking.vwskgeo.ste.model.Asset;
import com.tsystems.smarttracking.vwskgeo.ste.model.Event;
import com.tsystems.smarttracking.vwskgeo.ste.model.Position;
import com.tsystems.smarttracking.vwskgeo.ste.model.QuuppaEvent;
import com.tsystems.smarttracking.vwskgeo.ste.model.QuuppaZone;
import com.tsystems.smarttracking.vwskgeo.ste.repository.AreaRepository;
import com.tsystems.smarttracking.vwskgeo.ste.repository.AssetRepository;

@RestController
public class SmartTrackingEngineController {

    private final static String ZONE_IDENTIFIER = "Zone";

    private final static double CARTESIAN2WGS84_FACTOR = 0.00001;
    
    // Caches last device events 
    private ConcurrentMap<String, Event> lastEvent = new ConcurrentHashMap<String, Event>();
    private ConcurrentMap<String, Event> newEvent = new ConcurrentHashMap<String, Event>();
    private Boolean pushEventInProgress = new Boolean(false);

    // JSON object mapper
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private SocketHandler socketHandler;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private AssetRepository assetRepository;
    
    @RequestMapping(value = "/events", method = RequestMethod.POST)
    ResponseEntity<?> addEvent(@RequestBody QuuppaEvent quuppaEvent) {

        Event event = mapQuuppaEventToEvent(quuppaEvent);

        // Ignore events from unknown/anonymous sources
        // Ignore old events
        String source = event.getSource();
        if (source != null && !source.isEmpty()) {
            newEvent.compute(source, (k, v) -> v != null && v.getTimestamp() > event.getTimestamp() ? v : event);
            
            synchronized (pushEventInProgress) {
                if (!pushEventInProgress) {
                    pushEventInProgress = true;
                    pushEvent();
                }
            }
        }

        return ResponseEntity.noContent().build();
    }

    @Async
    public void pushEvent() {
        try {
            while (!newEvent.isEmpty()) {
                Set<String> sources = newEvent.keySet();
                lastEvent.putAll(newEvent);
                
                sources.forEach(source -> {
                    try {
                        byte[] eventAsBytes = objectMapper.writeValueAsBytes(lastEvent.get(source));
                        socketHandler.sendTextMessage(new TextMessage(eventAsBytes));
                    } catch (JsonProcessingException e) {
                        // Ignore malformed events
                    }
                    
                    newEvent.remove(source, lastEvent.get(source));
                });
            }
        }
        finally {
            synchronized (pushEventInProgress) {
                pushEventInProgress = false;
            }
        }
    }
    
    @RequestMapping(value = "/lastevents", method = RequestMethod.GET)
    public Collection<Event> lastEvents() {
        return lastEvent.values();
    }

    @RequestMapping(value = "/{source}/lastevent", method = RequestMethod.GET)
    public Event lastEvent(@PathVariable String source) {
        return lastEvent.get(source);
    }

    private Event mapQuuppaEventToEvent(QuuppaEvent quuppaEvent) {
        Event event = new Event();
        event.setSource(quuppaEvent.getName());
        event.setTagId(quuppaEvent.getId());
        event.setTagName(quuppaEvent.getName());
        Asset asset = assetRepository.findByTagId(event.getTagId()).orElse(new Asset("Unknown"));
        event.setAssetId(asset.getId());
        event.setAssetName(asset.getName() + " (" + event.getTagName() + ")");
        event.setTimestamp(quuppaEvent.getLastPacketTS());
        
        // Calculate WGS84 position
        List<Double> positionWgs84 = quuppaEvent.getSmoothedPositionWGS84();
        if (positionWgs84 != null) {
            event.setPosition(new Position(positionWgs84));
        }
        else {
            List<Double> cartesianPosition = quuppaEvent.getSmoothedPosition();
            Position position = null;
            if (cartesianPosition != null && (position = cartesianToWgs84(cartesianPosition.get(0), cartesianPosition.get(1))) != null) {
                event.setPosition(position);
            }
        }

        // Find the matching zone
        String zoneName = "Unknown";
        if (quuppaEvent.getZones() != null) {
            List<QuuppaZone> appZones = quuppaEvent.getZones().stream()
                    .filter(zone -> zone.getName().startsWith(ZONE_IDENTIFIER))
                    .collect(Collectors.toList());

            if (appZones.size() == 0) {
                zoneName = String.format("No match. Source event zones: %s", quuppaEvent.getZones().stream().map(zone -> zone.getName()).collect(Collectors.toList()));
            } else if (appZones.size() == 1) {
                zoneName = appZones.get(0).getName();
            } else {
                zoneName = String.format("Multiple matches, one of %s", appZones.stream().map(zone -> zone.getName()).collect(Collectors.toList()));
            }
        }
        event.setZone(zoneName);

        // Alarm
        event.setAlarm(quuppaEvent.isShock());
        
        // Temperature
        if (quuppaEvent.getTemperature() != QuuppaEvent.TEMPERATURE_NON_VALUE) {
            event.setTemperature(quuppaEvent.getTemperature());
            event.setTemperatureTS(quuppaEvent.getTemperatureTS());
        }

        return event;
    }
    
    private Position cartesianToWgs84(double x, double y)
    {
        Position position = null;
        Iterable<Area> it = areaRepository.findAll();
        if (it.iterator().hasNext()) {
            Position center = it.iterator().next().getCenter();
            position = new Position();
            position.setLatitude(round(center.getLatitude() + y * CARTESIAN2WGS84_FACTOR, 7));
            position.setLongitude(round(center.getLongitude() + x * CARTESIAN2WGS84_FACTOR, 7));
        }
        return position;
    }
    
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
