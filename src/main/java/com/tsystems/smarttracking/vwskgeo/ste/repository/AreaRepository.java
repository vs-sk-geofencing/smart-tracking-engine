package com.tsystems.smarttracking.vwskgeo.ste.repository;

import org.springframework.data.repository.CrudRepository;

import com.tsystems.smarttracking.vwskgeo.ste.model.Area;

public interface AreaRepository extends CrudRepository<Area, String> {
}
