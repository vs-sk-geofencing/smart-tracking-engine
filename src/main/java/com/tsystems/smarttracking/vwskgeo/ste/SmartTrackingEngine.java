package com.tsystems.smarttracking.vwskgeo.ste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableMongoRepositories("com.tsystems.smarttracking.vwskgeo.ste.repository")
public class SmartTrackingEngine
{
    public static void main(String[] args)
    {
        SpringApplication.run(SmartTrackingEngine.class, args);
    }
}
