package com.tsystems.smarttracking.vwskgeo.ste.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

@Controller
public class CustomErrorController implements ErrorController
{
    private static final String ERROR_PATH = "/error";

    @Autowired
    private ErrorAttributes     errorAttributes;

    @Override
    public String getErrorPath()
    {
        return ERROR_PATH;
    }

    @RequestMapping(ERROR_PATH)
    String error(WebRequest request, Model model)
    {
        Map<String, Object> errorMap = errorAttributes.getErrorAttributes(request, false);
        model.addAttribute("errors", errorMap);
        return "error";
    }
}
