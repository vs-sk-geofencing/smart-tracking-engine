package com.tsystems.smarttracking.vwskgeo.ste.model;

import java.util.List;

public class QuuppaEvent {
    public static double TEMPERATURE_NON_VALUE = -10000.0;

    private String name;
    private String id;
    private List<Double> smoothedPosition;
    private List<Double> smoothedPositionWGS84;
    private List<QuuppaZone> zones;
    private List<Integer> acceleration;
    private boolean shock;
    private long lastPacketTS;
    private double temperature = TEMPERATURE_NON_VALUE;
    private long temperatureTS;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<QuuppaZone> getZones() {
        return zones;
    }

    public void setZones(List<QuuppaZone> zones) {
        this.zones = zones;
    }

    public List<Integer> getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(List<Integer> acceleration) {
        this.acceleration = acceleration;
    }

    public boolean isShock() {
        return shock;
    }

    public void setShock(boolean shock) {
        this.shock = shock;
    }

    public long getLastPacketTS() {
        return lastPacketTS;
    }

    public void setLastPacketTS(long timestamp) {
        this.lastPacketTS = timestamp;
    }

    public List<Double> getSmoothedPosition() {
        return smoothedPosition;
    }

    public void setSmoothedPosition(List<Double> smoothedPosition) {
        this.smoothedPosition = smoothedPosition;
    }

    public List<Double> getSmoothedPositionWGS84() {
        return smoothedPositionWGS84;
    }

    public void setSmoothedPositionWGS84(List<Double> smoothedPositionWGS84) {
        this.smoothedPositionWGS84 = smoothedPositionWGS84;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public long getTemperatureTS() {
        return temperatureTS;
    }

    public void setTemperatureTS(long temperatureTS) {
        this.temperatureTS = temperatureTS;
    }
}
