package com.tsystems.smarttracking.vwskgeo.ste.repository;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tsystems.smarttracking.vwskgeo.ste.model.Area;
import com.tsystems.smarttracking.vwskgeo.ste.model.Position;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration
@EnableMongoRepositories("com.tsystems.smarttracking.vwskgeo.ste.repository")
public class AreaRepositoryTest {
    
    @Autowired
    private AreaRepository repo;
    
    @Test
    public void test() {
        Area area = repo.save(new Area("H9", new Position()));
        repo.save(new Area("H10", new Position()));
        assertTrue(repo.count() == 2);
        Optional<Area> opt = repo.findById(area.getId());
        assertTrue(opt.isPresent());
        assertEquals("H9", opt.get().getName());
        
        repo.deleteById(area.getId());
        opt = repo.findById(area.getId());
        assertFalse(opt.isPresent());
        assertTrue(repo.count() == 1);
        repo.deleteAll();
        assertTrue(repo.count() == 0);
    }
}
