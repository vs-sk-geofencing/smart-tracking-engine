package com.tsystems.smarttracking.vwskgeo.ste.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.tsystems.smarttracking.vwskgeo.ste.model.Asset;

@Component
public class AssetRepositoryMock implements AssetRepository {
    private static List<Asset> hardCodedAssets = new ArrayList<Asset>();
    static {
        Asset asset = new Asset("Audi 12345");
        hardCodedAssets.add(asset);
    }
    
    @Override
    public long count() {
        return 1;
    }

    @Override
    public void delete(Asset arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void deleteAll() {
        // TODO Auto-generated method stub
    }

    @Override
    public void deleteAll(Iterable<? extends Asset> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void deleteById(String arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean existsById(String arg0) {
        return true;
    }

    @Override
    public Iterable<Asset> findAll() {
        return hardCodedAssets;
    }

    @Override
    public Iterable<Asset> findAllById(Iterable<String> arg0) {
        return hardCodedAssets;
    }

    @Override
    public Optional<Asset> findById(String arg0) {
        return Optional.of(hardCodedAssets.get(0));
    }

    @Override
    public <S extends Asset> S save(S arg0) {
        return (S) hardCodedAssets.get(0);
    }

    @Override
    public <S extends Asset> Iterable<S> saveAll(Iterable<S> arg0) {
        return (Iterable<S>) hardCodedAssets;
    }

    @Override
    public Optional<Asset> findByTagId(String tagId) {
        return Optional.of(hardCodedAssets.get(0));
    }

}
