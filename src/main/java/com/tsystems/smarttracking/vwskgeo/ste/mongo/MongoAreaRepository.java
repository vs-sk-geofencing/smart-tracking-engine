package com.tsystems.smarttracking.vwskgeo.ste.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tsystems.smarttracking.vwskgeo.ste.model.Area;
import com.tsystems.smarttracking.vwskgeo.ste.repository.AreaRepository;

public interface MongoAreaRepository extends AreaRepository, MongoRepository<Area, String> {

}
