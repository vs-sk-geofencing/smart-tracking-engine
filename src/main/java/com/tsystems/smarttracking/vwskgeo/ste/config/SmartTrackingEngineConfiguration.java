package com.tsystems.smarttracking.vwskgeo.ste.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.tsystems.smarttracking.vwskgeo.ste.controller.SocketHandler;

@Configuration
@EnableWebSocket
public class SmartTrackingEngineConfiguration implements WebSocketConfigurer
{
    @Value("${web.socket.path}")
    private String websocketPath;
    
    @Autowired
    private SocketHandler socketHandler;
    
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(socketHandler, websocketPath).setAllowedOrigins("*");
    }
}
