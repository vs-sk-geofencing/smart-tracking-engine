package com.tsystems.smarttracking.vwskgeo.ste.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tsystems.smarttracking.vwskgeo.ste.model.Area;
import com.tsystems.smarttracking.vwskgeo.ste.repository.AreaRepository;

@RestController
public class AreaController {
    @Autowired
    private AreaRepository areaRepository;
    
    @RequestMapping(value = "/areas", method = RequestMethod.GET)
    public Collection<Area> areas() {
        Collection<Area> areas = new ArrayList<Area>();
        Iterable<Area> it = areaRepository.findAll();
        for (Area area : it) {
            areas.add(area);
        }
        return areas;
    }
}
