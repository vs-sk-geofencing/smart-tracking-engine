package com.tsystems.smarttracking.vwskgeo.ste.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.tsystems.smarttracking.vwskgeo.ste.model.Area;
import com.tsystems.smarttracking.vwskgeo.ste.model.Position;

//@Component
public class AreaRepositoryMock implements AreaRepository {
    private static List<Area> hardCodedAreas = new ArrayList<Area>();
    static {
        Area area = new Area();
        area.setId("0123456789");
        area.setName("H9");
        Position center = new Position();
        center.setLatitude(48.2323066);
        center.setLongitude(16.9895693);
        area.setCenter(center);
        hardCodedAreas.add(area);
    }
    
    @Override
    public List<Area> findAll() {
        return hardCodedAreas;
    }

    @Override
    public long count() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void delete(Area arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteAll() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteAll(Iterable<? extends Area> arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteById(String arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean existsById(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Iterable<Area> findAllById(Iterable<String> arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<Area> findById(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <S extends Area> S save(S arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <S extends Area> Iterable<S> saveAll(Iterable<S> arg0) {
        // TODO Auto-generated method stub
        return null;
    }

}
