package com.tsystems.smarttracking.vwskgeo.ste.model;

import java.util.List;

public class Position
{
    private double latitude;
    private double longitude;
    private float altitude;
    private float accuracy; // accuracy, meters

    public Position() {
        
    }
    
    public Position(List<Double> position) {
        if (position != null && position.size() >= 2) {
            this.latitude = position.get(0);
            this.longitude = position.get(1);
        }
    }
    
    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }
    
    public float getAccuracy()
    {
        return accuracy;
    }

    public void setAccuracy(float acc)
    {
        this.accuracy = acc;
    }

    @Override
    public String toString()
    {
        return "Latitude=" + latitude + ", longitude=" + longitude + ", accuracy=" + accuracy;
    }
}
