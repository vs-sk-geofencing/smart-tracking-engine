package com.tsystems.smarttracking.vwskgeo.ste.model;

import org.springframework.data.annotation.Id;

public class Area
{
    @Id
    private String id;
    private String name;
    private Position center;
    
    public Area() {
    }

    public Area(String name, Position center) {
        this.name = name;
        this.center = center;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Position getCenter() {
        return center;
    }
    public void setCenter(Position center) {
        this.center = center;
    }
    
    @Override
    public String toString()
    {
        return "id=" + id + ", name=" + name + ", center=" + center;
    }
}
