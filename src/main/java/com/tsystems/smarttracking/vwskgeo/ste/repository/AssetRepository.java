package com.tsystems.smarttracking.vwskgeo.ste.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.tsystems.smarttracking.vwskgeo.ste.model.Asset;

public interface AssetRepository extends CrudRepository<Asset, String> {
    public Optional<Asset> findByTagId(String tagId);
}
